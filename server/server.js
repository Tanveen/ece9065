require('rootpath')();
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var cors = require('cors');
var config = require('config.json');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//To secure the api use JWT auth
app.use(expressJwt({ secret: config.secret }).unless({ path: ['/users/authenticate', '/users/register'] }));

// This gives us routes
app.use('/users', require('./controllers/userlogin.controller'));

// start server
var port = process.env.NODE_ENV === 'production' ? 80 : 8000;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
